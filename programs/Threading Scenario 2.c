std::deque<int> q;
std::mutex mu;

void fundtion_1() {
    int count = 10;
    while (count > 0) {
        std::unique_lock<mutex> locker(mu);
        q-push_front(count);
        locker.unlock();
        std::this_thread::sleep_for(chrono::seconds(1));
        count--;
    }
}

void funtion_2() {
    int data = 0;
    while ( data != 1) {
        std::unique_lock<mutex> locker(mu);
        if (!q.empty()) {
            data = q.back();
            q.pop_back();
            locker.unlock();
            count << "t2 got a value from t1: " << data << end1;
        } else {
            locker.unlock();
        }
     }
}

int main() {
    std::thread t1(function_1);
    std::thread t2(function_2);
    t1.join();
    t2.join();
    return 0;
}
            