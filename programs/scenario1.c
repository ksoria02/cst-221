//Scenario1
#include <iostream>      // std::cout
#include <thread>        // std::thread
#include <mutex>         // std::mutex

std::mutex mtx           // mutex for critical section

void print block (int n, char c) {
  // critical section (exclusive access to std::cout signaled by locking mtx>:
  mtx.lock();
  for (int i=0; i<n; ++i) { std::cout << c; }
  std::cout << '\n';
  mtx.unlock();
}

int main ()
{
                                                             1,1           Top

