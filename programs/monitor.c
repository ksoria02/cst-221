#include  <thread.h>
#include  <synch.h>
#include  "inc-dec-m.h"

static  int  count;      
/* the static counter       */

static  mutex_t  MonitorLock;           /* the static mutex lock    */

/* ---------------------------------------------------------------- */
/* FUNCTION  CounterInit() :                                        */
/*      Initialize the simulated monitor.                           */
/* ---------------------------------------------------------------- */

void  CounterInit(int  n)
{
     count = n;
     mutex_init(&MonitorLock, USYNC_THREAD, (void *) NULL);
}

/* ---------------------------------------------------------------- */
/* FUNCTION  INC() :                                                */
/*      Increase the counter value by one.                          */
/* ---------------------------------------------------------------- */

int  INC(void)
{
     int  value;

     mutex_lock(&MonitorLock);          /* lock the monitor         */
          value = (++count);            /* increase and save counter*/
     mutex_unlock(&MonitorLock);        /* release the monitor      */
     return  value;                     /* return the new value     */
}

/* ---------------------------------------------------------------- */
/* FUNCTION  DEC() :                                                */
/*      Decrease the counter value by one.                          */
/* ---------------------------------------------------------------- */

int  DEC(void)
{
     int  value;

     mutex_lock(&MonitorLock);
          value = (--count);
     mutex_unlock(&MonitorLock);
     return  value;
}

/* ---------------------------------------------------------------- */
/* FUNCTION  SET() :                                                */
/*      Set the counter to a new value.                             */
/* ---------------------------------------------------------------- */

void  SET(int  n)
{
     mutex_lock(&MonitorLock);
          count = n;
     mutex_unlock(&MonitorLock);
}

/* ---------------------------------------------------------------- */
/* FUNCTION  GET() :                                                */
/*      Retrieve the counter's value.                               */
/* ---------------------------------------------------------------- */

int  GET(void)
{
     int  value;

     mutex_lock(&MonitorLock);
          value = count;
     mutex_unlock(&MonitorLock);
     return  value;
}

